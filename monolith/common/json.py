from json import JSONEncoder
from datetime import datetime
from typing import Any
from django.db.models import QuerySet
from presentations.models import Status


class StatusEncoder(JSONEncoder):
    def default(self, o: Any) -> Any:
        if isinstance(o, Status):
            return str(o)
        else:
            return super().default(o)


class QueryEncoder(JSONEncoder):
    def default(self, o: Any) -> Any:
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)


class ModelEncoder(StatusEncoder, QueryEncoder, DateEncoder, JSONEncoder):
    encoders = {}

    def default(self, o):
        #   if the object to decode is the same class as what's in the
        #   model property, then
        #     * create an empty dictionary that will hold the property names
        #       as keys and the property values as values
        #     * for each name in the properties list
        #         * get the value of that property from the model instance
        #           given just the property name
        #         * put it into the dictionary with that property name as
        #           the key
        #     * return the dictionary
        #   otherwise,
        #       return super().default(o)  # From the documentation

        if isinstance(o, self.model):
            d = {}
            if hasattr(o, "get_api_url"):
                d["href"] = o.get_api_url()
            for property in self.properties:
                val = getattr(o, property)
                if property in self.encoders:
                    encoder = self.encoders[property]
                    val = encoder.default(val)
                d[property] = val
            d.update(self.get_extra_data(o))
            return d
        else:
            return super().default(o)

    def get_extra_data(self, o):
        return {}
