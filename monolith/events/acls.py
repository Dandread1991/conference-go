from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    query = f"{city} {state}"
    url = f"https://api.pexels.com/v1/search?query={query}"
    response = requests.get(url, headers=headers)
    picture_url = response.json()["photos"][0]["src"]["original"]
    return {"picture_url": picture_url}


def get_latlon(location):
    params = {
        "q": f"{location.city},{location.state.abbreviation},USA",
        "appid": OPEN_WEATHER_API_KEY,
    }
    geo_url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(geo_url, params)
    lat = response.json()[0]["lat"]
    lon = response.json()[0]["lon"]

    return lat, lon


def get_weather(location):
    lat, lon = get_latlon(location)
    if lat is None or lon is None:
        return None

    params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }

    weather_url = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(weather_url, params)
    description = response.json()["weather"][0]["description"]
    temperature = response.json()["main"]["temp"]
    return {"temperature": temperature, "description": description}
