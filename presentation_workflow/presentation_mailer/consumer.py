import json
import pika
import django
import os
import sys
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval(ch, method, properties, body):
    json_body = json.loads(body)
    send_mail(
        "Your presentation has been accepted",
        f"{json_body['presenter_name']}, we're happy to tell you that your presentation {json_body['title']} has been accepted",
        "admin@conference.go",
        [f'{json_body["presenter_email"]}'],
    )


def process_rejection(ch, method, properties, body):
    json_body = json.loads(body)

    send_mail(
        "Your presentation has been rejected",
        f"{json_body['presenter_name']}, we're sorry to tell you that your presentation {json_body['title']} has been rejected",
        "admin@conference.go",
        [f'{json_body["presenter_email"]}'],
    )


while True:
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel2 = connection.channel()
    channel2.queue_declare(queue="presentation_approvals")
    channel2.queue_declare(queue="presentation_rejections")
    channel2.basic_consume(
        queue="presentation_approvals",
        on_message_callback=process_approval,
        auto_ack=True,
    )
    channel2.basic_consume(
        queue="presentation_rejections",
        on_message_callback=process_rejection,
        auto_ack=True,
    )
    channel2.start_consuming()
